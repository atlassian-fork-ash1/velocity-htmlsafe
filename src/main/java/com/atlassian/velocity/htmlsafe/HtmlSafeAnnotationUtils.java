package com.atlassian.velocity.htmlsafe;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.velocity.htmlsafe.introspection.AnnotationBoxedElement;
import com.google.common.collect.ImmutableSet;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utilities for working with the {@link HtmlSafe} annotation
 */
public final class HtmlSafeAnnotationUtils {
    public static final Annotation HTML_SAFE_ANNOTATION = HtmlSafeAnnotationFactory.getHtmlSafeAnnotation();
    public static final Set<Annotation> HTML_SAFE_ANNOTATION_COLLECTION = ImmutableSet.of(HTML_SAFE_ANNOTATION);

    @TenantAware(value = TenancyScope.TENANTLESS, comment = "Caches by class name whether toString() method is annotated with HtmlSafe or not")
    private static final Map<String, Boolean> htmlSafeToStringMethodByClassCache = new ConcurrentHashMap<String, Boolean>(1000);

    /**
     * Provides a mechanism for obtaining an instance of the HtmlSafe marker annotation.
     * <p>
     * This implementation uses a dummy interface with an annotated method declaration and reflectively retrieves an
     * instance from this interface.
     */
    private static class HtmlSafeAnnotationFactory {
        static Annotation getHtmlSafeAnnotation() {
            try {
                return HtmlSafeAnnotationHolder.class.getMethod("holder").getAnnotation(HtmlSafe.class);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }

        private static interface HtmlSafeAnnotationHolder {
            @HtmlSafe
            Object holder();
        }
    }

    private HtmlSafeAnnotationUtils() {
    }

    /**
     * Return true if the object has a toString method that has been annotated as HtmlSafe
     *
     * @param value Object to query
     * @return true if HTML safe
     */
    public static boolean hasHtmlSafeToStringMethod(Object value) {
        final Class<?> clazz = value.getClass();
        final String className = clazz.getName();
        Boolean result = htmlSafeToStringMethodByClassCache.get(className);
        if (result == null) {
            try {
                result = clazz.getMethod("toString").isAnnotationPresent(HtmlSafe.class);
                htmlSafeToStringMethodByClassCache.put(className, result);
            } catch (NoSuchMethodException e) {
                // All objects have a toString method
                throw new RuntimeException("Object does not have a toString method");
            }
        }
        return result;
    }

    /**
     * Determines whether an annotated value is htmlsafe (i.e should not be encoded during rendering)
     *
     * @param value Object to query
     * @return true if HTML safe
     */
    public static boolean isHtmlSafeValue(AnnotationBoxedElement<?> value) {
        return (hasHtmlSafeToStringMethod(value.unbox()) || containsAnnotationOfType(value.getAnnotationCollection(), HtmlSafe.class));
    }

    /**
     * Detects whether a collection contains an annotation of a particular type
     *
     * @param annotations    Collection to scan
     * @param annotationType Annotation type to detect
     * @return true if annotation type is found
     */
    public static boolean containsAnnotationOfType(Collection<Annotation> annotations, Class<? extends Annotation> annotationType) {
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(annotationType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Detects whether or not a symbol ends with "HTML", regardless of case.
     *
     * @param name the name to be examined
     * @return <tt>true</tt> if the name ended with "HTML", ignoring case
     */
    public static boolean endsWithHtmlIgnoreCase(String name) {
        int pos = name.length();
        if (pos < 4) {
            return false;
        }
        char c = name.charAt(--pos);
        if (c != 'l' && c != 'L') {
            return false;
        }
        c = name.charAt(--pos);
        if (c != 'm' && c != 'M') {
            return false;
        }
        c = name.charAt(--pos);
        if (c != 't' && c != 'T') {
            return false;
        }
        c = name.charAt(--pos);
        return c == 'h' || c == 'H';
    }
}
