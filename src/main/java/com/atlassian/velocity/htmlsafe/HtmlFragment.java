package com.atlassian.velocity.htmlsafe;

import com.atlassian.velocity.htmlsafe.introspection.BoxedValue;

/**
 * Simple wrapper class for adding HtmlSafe values directly to a Velocity context.
 */
public final class HtmlFragment implements BoxedValue {
    private final Object fragment;

    public HtmlFragment(Object fragment) {
        this.fragment = fragment;
    }

    @HtmlSafe
    public String toString() {
        return fragment.toString();
    }

    public Object unbox() {
        return fragment;
    }
}
