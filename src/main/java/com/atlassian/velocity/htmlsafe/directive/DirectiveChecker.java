package com.atlassian.velocity.htmlsafe.directive;

import org.apache.velocity.Template;

/**
 * Checks whether directives are present in templates.
 *
 * @since v1.1.1
 */
public interface DirectiveChecker {
    /**
     * Determines whether a directive with an specified name is present on a template instance.
     *
     * @param directiveName The name of the directive to look for.
     * @param template      The template where we will search for the directive.
     * @return true, if the directive has been defined on the template; otherwise, false.
     */
    boolean isPresent(String directiveName, Template template);
}
