package com.atlassian.velocity.htmlsafe;

import com.atlassian.velocity.htmlsafe.annotations.CollectionInheritable;
import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares that this method returns an object that does not require encoding if it is printed to a HTML document via
 * its {@link Object#toString()} method
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@ReturnValueAnnotation
@CollectionInheritable
public @interface HtmlSafe {
}
