package com.atlassian.velocity.htmlsafe;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.velocity.htmlsafe.introspection.MethodAnnotator;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * This method annotator will annotate various methods that are known to be HTML safe from library classes.
 * <p>
 * At present this annotator will annotate the htmlEncode method from <code>com.opensymphony.util.TextUtils</code> and
 * <code>com.opensymphony.webwork.util.WebWorkUtil</code>, the html method from
 * <code>org.apache.velocity.tools.generic.EscapeTool</code> as being HTML safe.
 */
public final class HtmlSafeClassAnnotator implements MethodAnnotator {
    @TenantAware(value = TenancyScope.TENANTLESS)
    private static final Map<String, Set<String>> HTML_ENCODE_CLASS_METHODS = ImmutableMap.<String, Set<String>>builder()
            .put("com.opensymphony.util.TextUtils", ImmutableSet.of("htmlEncode"))
            .put("org.apache.velocity.tools.generic.EscapeTool", ImmutableSet.of("html"))
            .put("com.opensymphony.webwork.util.WebWorkUtil", ImmutableSet.of("htmlEncode"))
            .put("com.opensymphony.webwork.util.VelocityWebWorkUtil", ImmutableSet.of("htmlEncode"))
            .build();

    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        return isSafeMethod(method)
                ? HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION_COLLECTION
                : ImmutableSet.<Annotation>of();
    }

    private boolean isSafeMethod(Method method) {
        final Set<String> safeMethods = HTML_ENCODE_CLASS_METHODS.get(method.getDeclaringClass().getName());
        return safeMethods != null && safeMethods.contains(method.getName());
    }
}
