package com.atlassian.velocity.htmlsafe.introspection;

import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.util.introspection.VelMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Velocity method that un-boxes the target object and any boxed arguments before calling the delegate method.
 * <p>
 * It will also process any list arguments, unboxing any list members that are {@link BoxedValue}s.
 */
final class UnboxingMethod implements VelMethod {
    private final VelMethod delegateMethod;

    public UnboxingMethod(VelMethod delegateMethod) {
        this.delegateMethod = checkNotNull(delegateMethod, "delegateMethod must not be null");
    }

    public Object invoke(Object o, Object[] objects) throws Exception {
        final Object[] unboxedArgs = BoxingUtils.unboxArrayElements(objects);
        unboxListArgumentElements(unboxedArgs);
        try {
            return delegateMethod.invoke(BoxingUtils.unboxObject(o), unboxedArgs);
        } catch (IllegalArgumentException e) {
            StringBuilder errorMessage = new StringBuilder();
            errorMessage.append("Failed to call ")
                    .append(BoxingUtils.unboxObject(o))
                    .append('.')
                    .append(delegateMethod.getMethodName())
                    .append(" with arguments : [")
                    .append(StringUtils.join(unboxedArgs, ", "))
                    .append("]");
            throw new IllegalArgumentException(errorMessage.toString(), e);
        }
    }

    public boolean isCacheable() {
        return delegateMethod.isCacheable();
    }

    public String getMethodName() {
        return delegateMethod.getMethodName();
    }

    public Class getReturnType() {
        return delegateMethod.getReturnType();
    }

    /**
     * Check an array for any element that implements {@link List) and for each list discovered, replace the list
     * with another list where any boxed members of the list have been unboxed.
     *
     * @param arguments Object array to process
     */
    private void unboxListArgumentElements(Object[] arguments) {
        for (int x = 0; x < arguments.length; x++) {
            if (arguments[x] instanceof List) {
                List unboxedList = new ArrayList((List) arguments[x]);
                ListIterator iterator = unboxedList.listIterator();
                while (iterator.hasNext()) {
                    iterator.set(BoxingUtils.unboxObject(iterator.next()));
                }
                arguments[x] = unboxedList;
            }
        }
    }
}
