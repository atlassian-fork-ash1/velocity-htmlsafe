package com.atlassian.velocity.htmlsafe.context;

import com.atlassian.velocity.htmlsafe.PossibleIncorrectHtmlEncodingEventHandler;
import org.apache.velocity.app.event.EventCartridge;

/**
 * Adds a {@link PossibleIncorrectHtmlEncodingEventHandler} to a cartridge if such logging is enabled.
 */
public final class IncorrectHtmlEncodingWarningProcessor implements EventCartridgeProcessor {
    public void processCartridge(EventCartridge cartridge) {
        if (PossibleIncorrectHtmlEncodingEventHandler.isLoggingEnabled())
            cartridge.addEventHandler(new PossibleIncorrectHtmlEncodingEventHandler());
    }
}
