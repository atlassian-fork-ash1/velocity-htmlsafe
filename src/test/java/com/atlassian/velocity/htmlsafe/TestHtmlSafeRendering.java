package com.atlassian.velocity.htmlsafe;

import com.atlassian.velocity.htmlsafe.introspection.HtmlSafeAnnotationBoxingUberspect;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.log.NullLogChute;
import org.junit.Before;
import org.junit.Test;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Integration tests for the HTML safe Velocity rendering system
 */

public class TestHtmlSafeRendering {
    private VelocityEngine velocityEngine;

    @Before
    public void setUp() throws Exception {
        velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.UBERSPECT_CLASSNAME, HtmlSafeAnnotationBoxingUberspect.class.getName());
        velocityEngine.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, NullLogChute.class.getName());
        velocityEngine.init();
    }

    @Test
    public void testSimpleEscaping() throws Exception {
        final Context context = new VelocityContext();
        context.put("foo", "<html></html>");
        context.put("myVal", new HtmlFragment("<script></script>"));
        context.put("barHtml", "Some html &amp; <em>text</em>");

        attachEscaper(context);
        final StringWriter writer = new StringWriter();

        final String expectedFooRendering = "&lt;html&gt;&lt;/html&gt;";
        final String expectedMyValRendering = "<script></script>";
        final String expectedBarHtmlRendering = "Some html &amp; <em>text</em>";

        velocityEngine.evaluate(context, writer, "test", "My test template $foo $myVal $barHtml");

        final String result = writer.toString();

        assertEquals("My test template " + expectedFooRendering + " " + expectedMyValRendering + " " + expectedBarHtmlRendering, result);
    }

    @Test
    public void testHtmlSafeMethodCallAnnotations() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "My test template $tester.htmlSafeString() $tester.plainString()");

        final String result = writer.toString();

        assertEquals("My test template <em>markup &amp; tigers</em> &lt;em&gt;markup &amp;amp; tigers&lt;/em&gt;", result);
    }

    @Test
    public void testHtmlSafePropertyGetAnnotations() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "My test template $tester.htmlSafeString $tester.plainString");

        final String result = writer.toString();

        assertEquals("My test template <em>markup &amp; tigers</em> &lt;em&gt;markup &amp;amp; tigers&lt;/em&gt;", result);
    }

    @Test
    public void testHtmlSafeIteration() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "#foreach ($val in $tester.splitAsHtmlSafe())$val#end");

        final String result = writer.toString();

        assertEquals("<em>markup&amp;tigers</em>", result);
    }

    @Test
    public void testNonHtmlSafeIteration() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "#foreach ($val in $tester.plainSplit())$val#end");

        final String result = writer.toString();

        assertEquals("&lt;em&gt;markup&amp;amp;tigers&lt;/em&gt;", result);
    }

    @Test
    public void testListMemberHtmlSafeInheritance() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "$tester.splitAsHtmlSafe().get(0) $tester.splitAsHtmlSafe().get(1)");

        final String result = writer.toString();

        assertEquals("<em>markup &amp;", result);
    }

    @Test
    public void testNonHtmlSafeListMemberEncoding() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "$tester.plainSplit().get(1) $tester.plainSplit().get(2)");

        final String result = writer.toString();

        assertEquals("&amp;amp; tigers&lt;/em&gt;", result);
    }

    @Test
    public void testMapEntryHtmlSafeInheritance() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "$tester.getHtmlSafeTextMap().get(1)");

        final String result = writer.toString();

        assertEquals("&amp;", result);
    }

    @Test
    public void testNonHtmlSafeMapEntryEncoding() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test", "$tester.getTextMap().get(2)");

        final String result = writer.toString();

        assertEquals("tigers&lt;/em&gt;", result);
    }

    @Test
    public void testProtectAgainsMaliciousRCE() throws Exception {
        final HtmlAnnotationTester tester = new HtmlAnnotationTester("<em>markup &amp; tigers</em>");

        final Context context = new VelocityContext();
        context.put("tester", tester);

        attachEscaper(context);

        final StringWriter writer = new StringWriter();

        velocityEngine.evaluate(context, writer, "test",
                "introspect #set($v=\"s\") $v.getClass().forName(\"com.atlassian.velocity.htmlsafe" +
                        ".TestHtmlSafeRendering\").getMethod(\"getForbiddenValue\").invoke(null)\n" +
                        "My test template $tester.htmlSafeString $tester.plainString");

        final String result = writer.toString();
        assertEquals("introspect  $v.getClass().forName(\"com.atlassian.velocity.htmlsafe.TestHtmlSafeRendering\")" +
                ".getMethod(\"getForbiddenValue\").invoke(null)\n" +
                "My test template <em>markup &amp; tigers</em> &lt;em&gt;markup &amp;amp; tigers&lt;/em&gt;", result);
    }

    @SuppressWarnings("unused")
    public static String getForbiddenValue() {
        return "ForbiddenValue";
    }

    private void attachEscaper(Context context) {
        final EventCartridge eventCartridge = new EventCartridge();
        eventCartridge.addReferenceInsertionEventHandler(new HtmlAnnotationEscaper());
        eventCartridge.attachToContext(context);
    }

    @SuppressWarnings("UnusedDeclaration")
    public static final class HtmlAnnotationTester {
        private final String returnString;

        public HtmlAnnotationTester(String returnString) {
            this.returnString = returnString;
        }

        @HtmlSafe
        public String htmlSafeString() {
            return returnString;
        }

        public String plainString() {
            return returnString;
        }

        @HtmlSafe
        public String getHtmlSafeString() {
            return returnString;
        }

        public String getPlainString() {
            return returnString;
        }

        @HtmlSafe
        public Collection splitAsHtmlSafe() {
            return plainSplit();
        }

        public Collection plainSplit() {
            return Arrays.asList(returnString.split("\\s+"));
        }

        @HtmlSafe
        public Map getHtmlSafeTextMap() {
            return getTextMap();
        }

        public Map getTextMap() {
            final Collection splitString = plainSplit();
            final Map<Integer, Object> textMap = new HashMap<>();
            int idx = 0;

            for (final Object item : splitString) {
                textMap.put(idx++, item);
            }

            return textMap;
        }

        public String getHtml() {
            return returnString;
        }

        public String renderBody() {
            return returnString;
        }
    }
}

