package com.atlassian.velocity.htmlsafe.introspection.deprecation;

/**
 * For use with {@link com.atlassian.velocity.htmlsafe.introspection.TestAnnotationBoxingUberspect}.
 * <p>
 * All methods are invoked through reflective calls.
 * </p>
 */
@SuppressWarnings({"deprecation", "UnusedDeclaration"})
public class ClassWithDeprecatedMethod {
    @Deprecated
    public String deprecatedMethod() {
        return "Success";
    }

    public DeprecatedClass getDeprecatedClass() {
        return new DeprecatedClass();
    }

    public String undeprecatedMethod() {
        return "Success";
    }
}
