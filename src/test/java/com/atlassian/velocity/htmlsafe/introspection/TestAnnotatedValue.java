package com.atlassian.velocity.htmlsafe.introspection;

import junit.framework.TestCase;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static com.google.common.collect.Iterables.elementsEqual;

/**
 * Tests for annotated values
 */
public class TestAnnotatedValue extends TestCase {
    public void testUnboxing() {
        Object value = new Object();
        AnnotatedValue annotatedValue = new AnnotatedValue(value, Collections.EMPTY_LIST);
        assertEquals("Unboxing returns original value", value, annotatedValue.unbox());
    }

    public void testIsAnnotationPresent() {
        Annotation testAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.TestAnnotation.class);

        AnnotatedValue annotatedValue = new AnnotatedValue(new Object(), Collections.singleton(testAnnotation));

        assertTrue("Associated annotation is detected", annotatedValue.isAnnotationPresent(Annotations.TestAnnotation.class));
        assertFalse("Unassociated annotation is not detected", annotatedValue.isAnnotationPresent(Annotations.AnotherTestAnnotation.class));
    }

    public void testGetAnnotation() {
        Annotation testAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.TestAnnotation.class);
        Annotation anotherTestAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.AnotherTestAnnotation.class);

        AnnotatedValue annotatedValue = new AnnotatedValue(new Object(), Arrays.asList(testAnnotation, anotherTestAnnotation));

        assertEquals("Associated annotation is retrieved correctly", testAnnotation, annotatedValue.getAnnotation(Annotations.TestAnnotation.class));
        assertEquals("Associated annotation is retrieved correctly", anotherTestAnnotation, annotatedValue.getAnnotation(Annotations.AnotherTestAnnotation.class));
        assertNull("Unassociated annotation is not retrievable", annotatedValue.getAnnotation(Annotations.YetAnotherTestAnnotation.class));
    }

    public void testBoxingStrategy() {
        Annotation testAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.TestAnnotation.class);
        AnnotatedValue annotatedValue = new AnnotatedValue(new Object(), Arrays.asList(testAnnotation));

        Object newValue = new Object();

        Object newBoxedValue = annotatedValue.box(newValue);

        assertTrue("New boxed value is of correct type", newBoxedValue instanceof AnnotatedValue);

        AnnotatedValue newAnnotatedValue = (AnnotatedValue) newBoxedValue;

        assertTrue("Existing annotations are boxed with new value", Arrays.equals(annotatedValue.getAnnotations(), newAnnotatedValue.getAnnotations()));
        assertEquals("New value is boxed correctly", newValue, newAnnotatedValue.unbox());
    }

    public void testGetAnnotations() {
        Annotation testAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.TestAnnotation.class);
        Annotation anotherTestAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.AnotherTestAnnotation.class);

        final Annotation[] annotationsForValue = new Annotation[]{testAnnotation, anotherTestAnnotation};
        AnnotatedValue annotatedValue = new AnnotatedValue(new Object(), Arrays.asList(annotationsForValue));

        assertTrue("Correct annotations are returned", elementsEqual(Arrays.asList(annotationsForValue), Arrays.asList(annotatedValue.getAnnotations())));
    }

    public void testGetCollectionInheritableAnnotations() {
        Annotation collectionInheritableAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.CollectionInheritableAnnotation.class);
        Annotation anotherTestAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.AnotherTestAnnotation.class);

        AnnotatedValue annotatedValue = new AnnotatedValue(new Object(), Arrays.asList(collectionInheritableAnnotation, anotherTestAnnotation));

        Collection<Annotation> inheritableAnnotations = annotatedValue.getCollectionInheritableAnnotations();
        assertEquals("Correct number of annotations returned", 1, inheritableAnnotations.size());
        assertEquals("Correct annotation returned", collectionInheritableAnnotation, inheritableAnnotations.iterator().next());
    }

    public void testEquals() {
        Annotation collectionInheritableAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.CollectionInheritableAnnotation.class);
        Annotation anotherTestAnnotation = TestAnnotationAnnotatedClass.class.getAnnotation(Annotations.AnotherTestAnnotation.class);

        Object value = new Object();

        Collection<Annotation> annotationList = Arrays.asList(collectionInheritableAnnotation, anotherTestAnnotation);
        List<Annotation> reverseAnnotationList = new LinkedList<Annotation>(annotationList);
        Collections.reverse(reverseAnnotationList);

        AnnotatedValue value1 = new AnnotatedValue(value, annotationList);
        AnnotatedValue value2 = new AnnotatedValue(value, reverseAnnotationList);

        assertEquals("Equal boxed values with different annotation iteration order are equal", value1, value2);

        AnnotatedValue value3 = new AnnotatedValue(new Object(), annotationList);
        assertFalse("Different boxed values with same annotations are not equal", value1.equals(value3));

        AnnotatedValue value4 = new AnnotatedValue(value, Collections.singleton(anotherTestAnnotation));
        assertFalse("Equal boxed value with different annotations are not equal", value1.equals(value4));
    }

    @Annotations.TestAnnotation
    @Annotations.AnotherTestAnnotation
    @Annotations.YetAnotherTestAnnotation
    @Annotations.CollectionInheritableAnnotation
    private static class TestAnnotationAnnotatedClass {
    }
}
