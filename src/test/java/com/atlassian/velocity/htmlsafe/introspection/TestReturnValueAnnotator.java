package com.atlassian.velocity.htmlsafe.introspection;

import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;
import junit.framework.TestCase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests for the ReturnValueAnnotator
 */
public class TestReturnValueAnnotator extends TestCase {
    public void testOnlyReturnValueAnnotationsAreReturned() throws NoSuchMethodException {
        MethodAnnotator methodAnnotator = new ReturnValueAnnotator();
        Method methodWithMixedAnnotations = ReturnValueAnnotationTestClass.class.getMethod("getterWithMixedAnnotations");

        Set<Annotation> expectedAnnotations = new HashSet<Annotation>(Arrays.asList(TestReturnValueAnnotationHolderClass.class.getAnnotations()));

        Collection<Annotation> returnValueAnnotations = methodAnnotator.getAnnotationsForMethod(methodWithMixedAnnotations);

        assertEquals("Correct number of annotations returned", 2, returnValueAnnotations.size());
        assertTrue("All expected annotations are returned", returnValueAnnotations.containsAll(expectedAnnotations));

        for (Annotation annotation : returnValueAnnotations) {
            assertNotNull("Only return value annotations are returned", annotation.annotationType().getAnnotation(ReturnValueAnnotation.class));
        }
    }

    private static class ReturnValueAnnotationTestClass {
        @Annotations.TestReturnValueAnnotation
        public Object getterWithAReturnValueAnnotations() {
            return new Object();
        }

        @Annotations.AnotherTestReturnValueAnnotation
        public Object getterWithAnotherReturnValueAnnotations() {
            return new Object();
        }

        public Object getterWithNoReturnValueAnnotations() {
            return new Object();
        }

        @Annotations.TestReturnValueAnnotation
        @Annotations.AnotherTestReturnValueAnnotation
        public Object getterWithTwoReturnValueAnnotations() {
            return new Object();
        }

        @Annotations.TestAnnotation
        @Annotations.TestReturnValueAnnotation
        @Annotations.AnotherTestReturnValueAnnotation
        public Object getterWithMixedAnnotations() {
            return new Object();
        }
    }

    @Annotations.TestReturnValueAnnotation
    @Annotations.AnotherTestReturnValueAnnotation
    private static class TestReturnValueAnnotationHolderClass {
    }
}




