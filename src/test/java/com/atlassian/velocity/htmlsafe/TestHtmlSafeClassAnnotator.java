package com.atlassian.velocity.htmlsafe;

import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.util.VelocityWebWorkUtil;
import junit.framework.TestCase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;

import static com.google.common.collect.Iterables.elementsEqual;

/**
 * Tests for the HtmlSafeClassAnnotator
 */
public class TestHtmlSafeClassAnnotator extends TestCase {
    public void testTargetMethodsAreAnnotatedAsSafe() throws Exception {
        HtmlSafeClassAnnotator classAnnotator = new HtmlSafeClassAnnotator();

        Method wwHtmlEncode = VelocityWebWorkUtil.class.getMethod("htmlEncode", Object.class);
        Method textUtilsHtmlEncode = TextUtils.class.getMethod("htmlEncode", String.class);

        Collection<Annotation> wwHtmlEncodeAnnotations = classAnnotator.getAnnotationsForMethod(wwHtmlEncode);
        Collection<Annotation> textUtilsHtmlEncodeAnnotations = classAnnotator.getAnnotationsForMethod(textUtilsHtmlEncode);
        Collection<Annotation> expectedAnnotations = HtmlSafeAnnotationUtils.HTML_SAFE_ANNOTATION_COLLECTION;

        assertNotNull(wwHtmlEncodeAnnotations);
        assertTrue(elementsEqual(expectedAnnotations, wwHtmlEncodeAnnotations));

        assertNotNull(textUtilsHtmlEncodeAnnotations);
        assertTrue(elementsEqual(expectedAnnotations, textUtilsHtmlEncodeAnnotations));

    }

    public void testNonTargetMethodsAreNotAnnotated() throws Exception {
        HtmlSafeClassAnnotator classAnnotator = new HtmlSafeClassAnnotator();

        Method nonHtmlEncode = Object.class.getMethod("toString");
        Method unknownHtmlEncode = UnknownHtmlEncoder.class.getMethod("htmlEncode", String.class);

        Collection<Annotation> nonHtmlEncodeAnnotations = classAnnotator.getAnnotationsForMethod(nonHtmlEncode);
        Collection<Annotation> unknownHtmlEncodeAnnotations = classAnnotator.getAnnotationsForMethod(unknownHtmlEncode);

        assertNotNull(nonHtmlEncodeAnnotations);
        assertTrue(nonHtmlEncodeAnnotations.isEmpty());

        assertNotNull(unknownHtmlEncodeAnnotations);
        assertTrue(unknownHtmlEncodeAnnotations.isEmpty());
    }

    private final static class UnknownHtmlEncoder {
        public static String htmlEncode(String string) {
            return string;
        }
    }
}
