package com.atlassian.velocity.htmlsafe;

import junit.framework.TestCase;

/**
 * Tests for RawVelocityReference
 */
public class TestRawVelocityReference extends TestCase {
    public void testSimpleReference() {
        RawVelocityReference reference = new RawVelocityReference("$myVar");
        assertEquals("myVar", reference.getBaseReferenceName());
        assertTrue(reference.isScalar());
    }

    public void testNonScalarReference() {
        RawVelocityReference reference = new RawVelocityReference("$action.getSomething()");
        assertFalse(reference.isScalar());
        assertEquals("action", reference.getBaseReferenceName());
    }

    public void testGetScalarComponentOnScalarReference() {
        RawVelocityReference reference = new RawVelocityReference("$myVar");
        RawVelocityReference scalarComponent = reference.getScalarComponent();
        assertEquals("myVar", scalarComponent.getBaseReferenceName());
        assertTrue(scalarComponent.isScalar());
    }

    public void testGetScalarComponentOnNonScalarReference() {
        RawVelocityReference reference = new RawVelocityReference("$something.somethingElse");
        RawVelocityReference scalarComponent = reference.getScalarComponent();
        assertEquals("something", scalarComponent.getBaseReferenceName());
        assertTrue(scalarComponent.isScalar());
    }

    public void testGetBaseReferenceNameWithCurlys() {
        RawVelocityReference reference = new RawVelocityReference("${myVar}");
        assertEquals("myVar", reference.getBaseReferenceName());
        assertTrue(reference.isScalar());
    }

    public void testGetBaseReferenceNameWithQuietNull() {
        RawVelocityReference reference = new RawVelocityReference("$!myVar");
        assertEquals("myVar", reference.getBaseReferenceName());
        assertTrue(reference.isScalar());
    }

    public void testGetBaseReferenceNameWithAllSugar() {
        RawVelocityReference reference = new RawVelocityReference("$!{myVar}");
        assertEquals("myVar", reference.getBaseReferenceName());
        assertTrue(reference.isScalar());
    }
}
